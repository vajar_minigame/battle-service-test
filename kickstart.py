import asyncio
import json
from dapr.conf import settings
from dapr.clients import DaprClient
from battle_test_actor_interface import BattleTestActorInterface
from dapr.actor import ActorProxy, ActorId

settings.DAPR_GRPC_PORT = 50006
settings.DAPR_HTTP_PORT = 3006


# do actor stuff
async def main():
    with DaprClient() as d:

        # Create a typed message with content type and body

        # Create a typed message with content type and body
        resp = await d.invoke_method(
            app_id="test-battle", http_verb="POST", method_name="api/v1.0/testBattle", data=None
        )

        # Print the response
        print(resp.content_type, flush=True)
        print(json.loads(resp.text()), flush=True)


print("hallo welt")

asyncio.run(main())
