from Models.cloud_event import CloudEvent
from Models.attack_command import AttackCommand
from Models.test_battle_state import BattleTestState
from Models.battle_events import BattleEndedEvent, BattleEvent, BattleStartedEvent, BattleTurnQueueEvent
from Models.battle import Battle
from Models.team import ExtendedTeam, Team
from Models.monster import BattleValues, BodyValues, Monster
import datetime

from dapr.actor import Actor, Remindable
from dapr.clients import DaprClient
from battle_test_actor_interface import BattleTestActorInterface
import json


class BattleTestActor(Actor, BattleTestActorInterface, Remindable):
    """Implements DemoActor actor service

    This shows the usage of the below actor features:

    1. Actor method invocation
    2. Actor state store management
    3. Actor reminder
    4. Actor timer
    """

    def __init__(self, ctx, actor_id):
        super(BattleTestActor, self).__init__(ctx, actor_id)
        self.actor_id = actor_id
        self.state_name = "mydata"

    async def _on_activate(self) -> None:
        """An callback which will be called whenever actor is activated."""
        print(f"Activate {self.__class__.__name__} actor!", flush=True)

    async def _on_deactivate(self) -> None:
        """An callback which will be called whenever actor is deactivated."""
        print(f"Deactivate {self.__class__.__name__} actor!", flush=True)

    async def get_my_data(self) -> object:
        """An actor method which gets mydata state value."""
        has_value, val = await self._state_manager.try_get_state(self.state_name)
        print(f"has_value: {has_value}", flush=True)
        return val

    async def set_my_data(self, data) -> None:
        """An actor method which set mydata state value."""
        print(f"set_my_data: {data}", flush=True)
        data["ts"] = datetime.datetime.now(datetime.timezone.utc)
        await self._state_manager.set_state("mydata", data)
        await self._state_manager.save_state()

    async def set_reminder(self, enabled) -> None:
        """Enables and disables a reminder.

        Args:
            enabled (bool): the flag to enable and disable demo_reminder.
        """
        print(f"set reminder to {enabled}", flush=True)
        if enabled:
            # Register 'demo_reminder' reminder and call receive_reminder method
            await self.register_reminder(
                "demo_reminder",  # reminder name
                b"reminder_state",  # user_state (bytes)
                datetime.timedelta(seconds=5),  # The amount of time to delay before firing the reminder
                datetime.timedelta(seconds=5),
            )  # The time interval between firing of reminders
        else:
            # Unregister 'demo_reminder'
            await self.unregister_reminder("demo_reminder")
        print(f"set reminder is done", flush=True)

    async def start_battle(self):
        print(f"the battle {self.actor_id} will be started soon tm", flush=True)
        dapr_client = DaprClient()

        await dapr_client.invoke_method(
            "service-battle", f"api/v1.0/battle/{self.actor_id}/start", http_verb="POST", data=None
        )

    async def get_next_alive_mon(self, team: ExtendedTeam) -> int:
        for mon in team.monsters:
            if mon.battle_values.remaining_hp > 0:
                return mon.id
        print("no mon is alive in this team anymore", flush=True)

    async def receive_battle_event(self, event: CloudEvent):
        print("received Event", flush=True)
        print(event, flush=True)
        event = CloudEvent.parse_raw(event)
        if type(event.data) == BattleStartedEvent:
            team = event.data.battle.teams[0]

            await self._state_manager.set_state(
                self.state_name, BattleTestState(winning_team=team.id, winning_user=team.users[0]).dict()
            )
            await self._state_manager.save_state()
        if type(event.data) == BattleTurnQueueEvent:
            pass
        if type(event.data) == BattleEndedEvent:
            print("the battle has ended")
            has_value, val = await self._state_manager.try_get_state(self.state_name)
            if not has_value:
                raise AssertionError("The test setup is wrong")
            testState = BattleTestState(**val)
            if testState.winning_user != event.data.winner.users[0].id:
                raise AssertionError(
                    f"Expected user {testState.winning_user} to win, but {event.data.winner.users[0].id} won"
                )

            print("Test succeded")
            return
        battle = event.data.battle

        # get next mon
        # attack always the second team; the first mon that is alive
        nextmon = battle.active.turn_queue[0]
        target = await self.get_next_alive_mon(battle.teams[1])
        cmd = AttackCommand(source=nextmon, target=target)
        dapr_client = DaprClient()

        await dapr_client.invoke_method(
            "service-battle",
            f"api/v1.0/battle/{self.actor_id}/command/attack",
            http_verb="POST",
            data=cmd.json(by_alias=True),
        )

    async def receive_reminder(
        self,
        name: str,
        state: bytes,
        due_time: datetime.timedelta,
        period: datetime.timedelta,
    ) -> None:
        """A callback which will be called when reminder is triggered."""
        print(f"receive_reminder is called - {name} reminder - {str(state)}", flush=True)
