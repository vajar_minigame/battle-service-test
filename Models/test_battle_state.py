from typing import List
from pydantic import BaseModel


class BattleTestState(BaseModel):
    winning_team: int
    winning_user: int
    is_finished: bool = False
