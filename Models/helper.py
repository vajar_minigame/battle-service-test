def to_camel(string: str) -> str:
    camel_case_text = "".join(word.capitalize() for word in string.split("_"))
    lowerCamel = camel_case_text[0].lower() + camel_case_text[1:]
    return lowerCamel
