from Models.battle import Battle, ExtendedBattle
from Models.helper import to_camel
from pydantic import BaseModel
from Models.team import AddTeamRequest, Team
from typing import List, Literal, Optional, Union


class BattleEventBase(BaseModel):
    battle_id: int

    class Config:
        alias_generator = to_camel


class BattleStartedEvent(BattleEventBase):

    event_type: Literal["BattleStarted"]
    battle: ExtendedBattle

    class Config:
        alias_generator = to_camel


class BattleTurnQueueEvent(BattleEventBase):
    event_type: Literal["TurnQueue"]
    battle: ExtendedBattle

    class Config:
        alias_generator = to_camel


class BattleEndedEvent(BattleEventBase):
    event_type: Literal["BattleEnded"]
    winner: Team

    class Config:
        alias_generator = to_camel


BattleEvent = Union[BattleStartedEvent, BattleTurnQueueEvent, BattleEndedEvent]
