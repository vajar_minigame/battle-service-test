from Models.helper import to_camel
from pydantic import BaseModel


class AttackCommand(BaseModel):
    source: int
    target: int
