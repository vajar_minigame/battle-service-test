from Models.monster import Monster, MonsterId, UserId
from typing import List
from pydantic import BaseModel


class AddTeamRequest(BaseModel):
    monsters: List[int]
    users: List[int]


class Team(BaseModel):
    monsters: List[MonsterId]
    users: List[UserId]


class ExtendedTeam(BaseModel):
    id: int
    monsters: List[Monster]
    users: List[int]
