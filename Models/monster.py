from Models.helper import to_camel
from typing import Optional
from pydantic import BaseModel


class UserId(BaseModel):
    id: int


class MonsterId(BaseModel):
    id: int


class BattleValues(BaseModel):
    attack: int
    defense: int
    max_hp: int
    remaining_hp: int

    class Config:
        alias_generator = to_camel


class BodyValues(BaseModel):
    remaining_saturation: int
    max_saturation: int
    mass: int

    class Config:
        alias_generator = to_camel


class Monster(BaseModel):
    id: Optional[int]
    name: str
    user_id: int
    battle_values: BattleValues

    class Config:
        alias_generator = to_camel
