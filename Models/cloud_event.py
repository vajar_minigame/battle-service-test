from Models.battle_events import BattleEndedEvent, BattleEvent, BattleStartedEvent, BattleTurnQueueEvent
from typing import Union
from Models.battle import Battle
from pydantic import BaseModel


class CloudEvent(BaseModel):
    id: str
    source: str
    data: BattleEvent
    datacontenttype: str
