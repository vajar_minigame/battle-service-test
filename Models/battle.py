from Models.helper import to_camel
from pydantic import BaseModel
from Models.team import AddTeamRequest, ExtendedTeam, Team
from typing import List, Optional
import datetime


class AddBattleRequest(BaseModel):
    id: Optional[int]
    teams: List[AddTeamRequest]
    is_test: bool = True

    class Config:
        alias_generator = to_camel


class Battle(BaseModel):
    id: Optional[int]
    teams: List[Team]
    is_test: bool

    class Config:
        alias_generator = to_camel


class ActiveBattle(BaseModel):
    start_time: datetime.datetime
    turn_count: int
    turn_queue: List[int]
    last_action: datetime.datetime

    class Config:
        alias_generator = to_camel


class ExtendedBattle(BaseModel):
    id: Optional[int]
    teams: List[ExtendedTeam]
    active: ActiveBattle
    is_test: bool

    class Config:
        alias_generator = to_camel
