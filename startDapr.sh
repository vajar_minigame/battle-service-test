daprd --app-id test-battle \
    --app-port 5006 \
    --dapr-http-port 3006 \
    --dapr-grpc-port 50006 \
    --components-path "./components" \
    --placement-host-address "localhost:50005" \
    --config ./tracing.yaml \
    --metrics-port 9096 \
    --log-level debug
