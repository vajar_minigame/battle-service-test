from Models.battle_events import BattleEvent
from dapr.actor import ActorInterface, actormethod


class BattleTestActorInterface(ActorInterface):
    @actormethod(name="SetReminder")
    async def set_reminder(self, enabled: bool) -> None:
        ...

    @actormethod(name="StartBattle")
    async def start_battle(self) -> None:
        ...

    @actormethod(name="ReceiveBattleEvent")
    async def receive_battle_event(self, event: BattleEvent) -> None:
        ...
