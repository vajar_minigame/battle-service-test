from Models.battle_events import BattleEndedEvent, BattleStartedEvent, BattleTurnQueueEvent
from dapr.actor.client.proxy import ActorProxy
from dapr.actor.id import ActorId
from battle_test_actor_interface import BattleTestActorInterface
from Models.cloud_event import CloudEvent
from Models.battle import AddBattleRequest, Battle
from Models.team import AddTeamRequest, Team
from battle_test_actor import BattleTestActor
from dapr.conf import settings
from fastapi import FastAPI  # type: ignore
from dapr.ext.fastapi import DaprActor  # type: ignore
import json
from dapr.clients import DaprClient
from Models.monster import BattleValues, BodyValues, Monster
import uvicorn
from dapr.conf import global_settings
from fastapi.exceptions import RequestValidationError
from fastapi import FastAPI, Request, status
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder

settings.DAPR_GRPC_PORT = 50006
settings.DAPR_HTTP_PORT = 3006

app = FastAPI(title=f"{BattleTestActor.__name__}Service")

actor = DaprActor(app)

# This route is optional.
@app.get("/")
def index():
    return {"status": "ok"}


@app.get("/dapr/subscribe")
def subscribeInfo():
    test = [
        {"pubsubname": "pubsub", "topic": "Battle/BattleTurnQueueEvent", "route": "/battleEvent"},
        {"pubsubname": "pubsub", "topic": "Battle/BattleEndedEvent", "route": "/battleEvent"},
        {"pubsubname": "pubsub", "topic": "Battle/BattleStartedEvent", "route": "/battleEvent"},
    ]
    return test


@app.post("/api/v1.0/testBattle", status_code=201)
async def create_battle() -> None:

    dapr_client = DaprClient()
    print("battlestart", flush=True)
    # create user
    player1 = {"username": "testplayer1", "isTest": True}
    player2 = {"username": "testplayer2", "isTest": True}
    resp1 = await dapr_client.invoke_method(
        app_id="service-user", method_name="api/v1.0/user", http_verb="POST", data=json.dumps(player1)
    )
    print(json.loads(resp1.text()), flush=True)
    resp2 = await dapr_client.invoke_method(
        app_id="service-user", method_name="api/v1.0/user", http_verb="POST", data=json.dumps(player2)
    )
    print(json.loads(resp1.text())["id"], flush=True)
    player_ids = [json.loads(resp1.text())["id"], json.loads(resp2.text())["id"]]

    team1 = AddTeamRequest(monsters=[], users=[player_ids[0]])
    team2 = AddTeamRequest(monsters=[], users=[player_ids[1]])
    teams = [team1, team2]

    # create monsters
    for i, id in enumerate(player_ids):

        for _ in range(2):
            defaultMon = Monster(
                id=None,
                name="testmon",
                userId=id,
                battleValues=BattleValues(attack=10, defense=10, maxHp=100, remainingHp=100),
                bodyValues=BodyValues(remainingSaturation=100, maxSaturation=100, mass=100),
            )

            monResp = await dapr_client.invoke_method(
                app_id="service-monster",
                method_name="api/v1.0/monster",
                http_verb="POST",
                data=defaultMon.json(by_alias=True),
            )
            print(json.loads(monResp.text()), flush=True)
            teams[i].monsters.append(json.loads(monResp.text())["id"])

    # create battle

    battle_request = AddBattleRequest(teams=teams, isTest=True)

    battleResp = await dapr_client.invoke_method(
        app_id="service-battle",
        method_name="api/v1.0/battle",
        http_verb="POST",
        data=battle_request.json(by_alias=True),
    )
    battle_rest_json=(json.loads(battleResp.text()))
    proxy = ActorProxy.create("BattleTestActor", ActorId(str(battle_rest_json["id"])), BattleTestActorInterface)
    await proxy.StartBattle()




@app.post("/battleEvent")
async def mytopic(request: Request) -> None:
    requestContent=await request.receive()
    cloudEvent=CloudEvent.parse_raw(requestContent["body"])
    print(cloudEvent, flush=True)
    proxy = ActorProxy.create("BattleTestActor", ActorId(str(cloudEvent.data.battle_id)), BattleTestActorInterface)
    await proxy.ReceiveBattleEvent(cloudEvent.json(by_alias=True))


@app.on_event("startup")
async def startup_event():
    # Register DemoActor
    print("start this stuff yo")
    await actor.register_actor(BattleTestActor)


if __name__ == "__main__":
    uvicorn.run(app, host="localhost", port=5006)
