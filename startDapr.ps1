$AppPort=5003


daprd --app-id 'test-battle' `
    --app-port $AppPort `
    --dapr-http-port 3005 `
    --dapr-grpc-port 50003 `
    --placement-host-address 'localhost:6050' `
    --app-protocol http `
    --metrics-port 9003 `
    --components-path 'C:\Users\diezf\.dapr\components' `
    --log-level debug
#uvicorn main:app --port 5003 --reload